//1
(() => {
  const handleChange = function (e) {
    const str = e.target.value;
		const arr = str.split(',');
		let rez = arr.reduce(( sum, elem ) => sum + (Number(elem)), 0 );

		console.log('Задача 1, arr', arr);
		console.log('Задача 1, rez', rez);
    };

    document.getElementById('task-one').onchange = handleChange;
})();
  