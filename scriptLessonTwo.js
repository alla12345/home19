//2--------
(() => {
  const handleInput = function (e) {
    const str = e.target.value;

		let newStr = '';
		for (let i = 0; i < str.length; i++){
			if (isNaN(Number(str[i]))) {
				newStr = newStr + str[i];
			}
		}

		e.target.value = newStr;
		// e.target.value = str.replace(/[0-9]/g, '');
    console.log('Задача 2,', e.target.value);
  };
  
  document.getElementById('task-two').oninput = handleInput;
})();
